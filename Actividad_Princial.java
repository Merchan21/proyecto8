package semanactualapp.ronaldmerchan.facci.practica8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Actividad_Princial extends AppCompatActivity implements View.OnClickListener{
    TextView cajaCedula, cajaNombres, cajaApeliidos, cajaDatos;
    Button botonLeer, botonEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad__princial);
        cajaCedula=(TextView) findViewById(R.id. txtCedulaMi);
        cajaApeliidos=(TextView)findViewById(R.id. txtApellidosMi);
        cajaNombres=(TextView)findViewById(R.id. txtNombresMi);
        cajaDatos=(TextView)findViewById(R.id. txtDatosMi);

        botonEscribir = (Button)findViewById(R.id. btnEscribirMi);
        botonLeer = (Button)findViewById(R.id. btnLeerMi);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribirMi:
                try{
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt",Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString()+","+cajaNombres.getText().toString());
                    escritor.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                break;
            case R.id.btnLeerMi:
                try{
                    BufferedReader lector =new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos =lector.readLine();
                    String[] listaPersonas =datos.split(";");
                    for(int i = 0; i < listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i].split(",")[0]+""+listaPersonas[i].split(",")[1]+""+listaPersonas[i].split(",")[2]);
                    }
                    //cajaDatos.setText("");
                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error en la lectura del archivo"+ex.getMessage());
                }
                break;
        }

    }
}
